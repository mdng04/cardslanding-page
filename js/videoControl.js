//Video Player Steuerung mit Buttons und EventListener
var vid_glossar = document.getElementById("vid_glossar");
//Integrierte Funktion play nutzen zu abspielen des Video
document.getElementById("play").addEventListener('click', function() { vid_glossar.play(); }, false);
//Integrierte Funktion pause nutzen zu pausieren des Video
document.getElementById('pause').addEventListener('click', function() { vid_glossar.pause(); }, false);
//Integrierte Funktion load nutzen zu reseten des Video
document.getElementById('reload').addEventListener('click', function() { vid_glossar.load(); }, false);
//Integrierte Funktion duration zum anzeigen der Länge des Video
document.getElementById('duration').addEventListener('click', function() {
    document.getElementById('ausgabe').innerHTML = 'Die Dauer des Videos beträgt: '+ vid_glossar.duration +' Sekunden.'}, false);

//Siehe Kommentare oben -^
var vid_Timer = document.getElementById("vid_timer");
document.getElementById("play1").addEventListener('click', function() { vid_Timer.play(); }, false);
document.getElementById('pause1').addEventListener('click', function() { vid_Timer.pause(); }, false);
document.getElementById('reload1').addEventListener('click', function() { vid_Timer.load(); }, false);
document.getElementById('duration1').addEventListener('click', function() {
    document.getElementById('ausgabe1').innerHTML = 'Die Dauer des Videos beträgt: '+ vid_Timer.duration +' Sekunden.'}, false);